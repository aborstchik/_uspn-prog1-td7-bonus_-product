=====================================================================
                             PRODUCT
=====================================================================


Principe
_____________________________________________________________________

Calcule et affiche le produit des éléments d'un tableau d'entiers de longueur quelconque initialisé avec des valeurs quelconques.
_____________________________________________________________________


Protocole
_____________________________________________________________________

On utilise une boucle for.
_____________________________________________________________________


Fichiers
_____________________________________________________________________

product.c: code source du projet

VERSION.txt: version du projet

Makefile
_____________________________________________________________________


Compilation et exécution (Linux)
_____________________________________________________________________

Pour compiler, ouvrir un terminal, se déplacer dans le répertoire du projet et entrer:
>make

Le programme peut être exécuté après compilation avec:
>./product.out
_____________________________________________________________________


Informations supplémentaires
_____________________________________________________________________

Réalisé dans le cadre d'un module de programmation de la licence d'informatique de l'Université Paris XIII.

GitLab:
https://gitlab.com/aborstchik/_uspn-prog1-td7-bonus_-product.git
_____________________________________________________________________

2021 - Creative Commons (CC0)
Alexis BORSTCHIK
