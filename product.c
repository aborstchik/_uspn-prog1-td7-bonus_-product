#include <stdlib.h>
#include <stdio.h>

#define VERBOSE 1

int main()
{
  //Initialization
  //__________________________________________________________
  int tab[]= {1, -1, 8, 6, -2};
  const int SIZE= 5;

  int i= 0;

  long product= 1;
  //__________________________________________________________


  //Computations
  //__________________________________________________________
  for(i=0; i<SIZE; i++)
    {
      product*= tab[i];
    }
  //__________________________________________________________


  //Post-process
  //__________________________________________________________
  if(VERBOSE)
    printf("Le produit des entiers du tableau est: ");
  
  printf("%ld\n", product);
  //__________________________________________________________
  

  return EXIT_SUCCESS;
}
