CC=gcc
CFLAGS=-Wall

exe=product.out
obj=product.o

.PHONY: all
all: $(exe)

$(exe): $(obj)
	$(CC) $(CFLAGS) $^ -o $@

$(obj): %.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clear
clear:
	$(RM) $(exe) $(obj)

.PHONY: gitadd
gitadd: clear
	git add *[!~#]
	git status
